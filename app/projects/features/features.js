(function() {
    'use strict';

    angular.module('App.Projects.Features', [
        'ui.bootstrap',
        'App.Template.Modal',
        'App.Service.Feature'
    ])
        .config(function($stateProvider){
            $stateProvider.state('features', {
                url: '/projects/:projectId/features',
                templateUrl: 'app/projects/features/features.html',
                controller: 'FeaturesController'
            });
        })
        .controller('FeaturesController', function ($scope, $stateParams, $filter, $uibModal, FeatureService) {
            $scope.projectId = $stateParams.projectId;
            $scope.sortType = 'name'; // set the default sort type
            $scope.sortReverse = false;  // set the default sort order
            $scope.searchProject = '';  // set the default search/filter term

            // get the list of features
            FeatureService.getFeatures($stateParams.projectId)
                .then(function(result) {
                    $scope.features = result.data.features;
                });
            
            FeatureService.getFeaturesById(1);

            $scope.showModal = function(title, name, id) {
                var data = {
                    title: title,
                    name: name,
                    id: id
                };
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/common/template/modal.html',
                    controller: 'ModalInstanceCtrl',
                    size: 'md',
                    resolve: {
                        data: data
                    }
                });

                modalInstance.result.then(function (data) {
                    // Trigger when Edit/Save button on modal is click
                    if (data.id) {
                        FeatureService.updateFeaturesById($stateParams.projectId,data.id,data.name)
                            .then(function(){
                                    FeatureService.getFeatures($stateParams.projectId)
                                        .then(function(result) {
                                            $scope.features = result.data.features;
                                        });
                            });
                    } else {
                        FeatureService.addFeaturesById($stateParams.projectId,data.name)
                            .then(function(){
                                            FeatureService.getFeatures($stateParams.projectId)
                                                .then(function(result) {
                                                    $scope.features = result.data.features;
                                                });
                            });
                    }
                       
                });
            };



                 $scope.selectedArray = [];

                $scope.CheckSelected = function (id) {
                    $scope.selectedArray.push(id);
                };

                $scope.removeSelected = function(){
                console.log($scope.selectedArray);
                FeatureService.deleteFeaturesById($stateParams.projectId,2)
                    .then(function(){
                        FeatureService.getFeatures($stateParams.projectId)
                            .then(function(result) {
                                $scope.features = result.data.features;
                            });
                    });
                }



        });
}());