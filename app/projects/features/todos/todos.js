(function() {
    'use strict';

    angular.module('App.Projects.Features.Todos', [
        'ui.bootstrap',
        'App.Template.Modal',
        'App.Service.Todo'
    ])
        .config(function($stateProvider){
            $stateProvider.state('todos', {
                url: '/projects/:projectId/features/:featureId/todos',
                templateUrl: 'app/projects/features/todos/todos.html',
                controller: 'TodosController'
            });
        })
        .controller('TodosController', function ($scope, $stateParams, $filter, $uibModal, TodoService) {
            $scope.projectId = $stateParams.projectId;
            $scope.featureId = $stateParams.featureId;

            $scope.sortType = 'name'; // set the default sort type
            $scope.sortReverse = false;  // set the default sort order
            $scope.searchProject = '';  // set the default search/filter term
  
            // get the list of todos
            TodoService.getTodos($stateParams.projectId,$stateParams.featureId)
                .then(function(result) {
                    $scope.todos = result.data.todos;
                });

                $scope.showModal = function(title, name, id) {
                var data = {
                    title: title,
                    name: name,
                    id: id
                };
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/common/template/modal.html',
                    controller: 'ModalInstanceCtrl',
                    size: 'md',
                    resolve: {
                        data: data
                    }
                });

                modalInstance.result.then(function (data) {
                    // Trigger when Edit/Save button on modal is click
                    if (data.id) {
                        TodoService.updateTodosById($stateParams.projectId,$stateParams.featureId,data.id,data.name)
                            .then(function(){
                                    TodoService.getTodos($stateParams.projectId,$stateParams.featureId)
                                        .then(function(result) {
                                            $scope.todos = result.data.todos;
                                        });
                            });
                    } else {
                        TodoService.addTodosById($stateParams.projectId,$stateParams.featureId,data.name)
                            .then(function(){
                                            TodoService.getTodos($stateParams.projectId,$stateParams.featureId)
                                                .then(function(result) {
                                                    $scope.todos = result.data.todos;
                                                });
                            });
                    }
                       
                });
            };


                $scope.selectedArray = [];

                $scope.CheckSelected = function (id) {
                    $scope.selectedArray.push(id);
                };

                $scope.removeSelected = function(){
                console.log($scope.selectedArray);
                TodoService.deleteTodosById($stateParams.projectId,$stateParams.featureId,1)
                    .then(function(){
                        TodoService.getTodos($stateParams.projectId,$stateParams.featureId)
                            .then(function(result) {
                                $scope.todos = result.data.todos;
                            });
                    });
                }




        });
}());