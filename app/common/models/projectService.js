(function() {
    'use strict';

    angular.module('App.Service.Project', [
        
    ])
        .service('ProjectService', function($http) {
            var URLS = {
                FETCH_ALL: 'http://localhost/todo-app/api/v1/projects',
                FETCH_BY_ID: 'http://localhost/todo-app/api/v1/projects/%d',
                UPDATE_BY_ID: 'http://localhost/todo-app/api/v1/projects/%d?name=%s',
                STORE_NEW: 'http://localhost/todo-app/api/v1/projects?name=%s',
                DELETE_BY_ID: 'http://localhost/todo-app/api/v1/projects/%s'
            };
            this.getProjects = function() {
                return $http.get(URLS.FETCH_ALL);
            };
            this.getProjectsById = function(id) {
                var url = sprintf(URLS.FETCH_BY_ID, id);
                return $http.get(url);
            };
            this.updateProjectsById = function(id,name){
                var url = sprintf(URLS.UPDATE_BY_ID, id, name);
                return $http.put(url);                
            };
            this.addProjectsById = function(name){
                var url = sprintf(URLS.STORE_NEW, name);
                return $http.post(url);                
            };
            this.deleteProjectsById = function(ids){
                 var url = sprintf(URLS.DELETE_BY_ID, ids);
                return $http.delete(url); 
            };

        });
}());