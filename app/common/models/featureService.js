(function() {
    'use strict';

    angular.module('App.Service.Feature', [
        
    ])
        .service('FeatureService', function($http) {
            var URLS = {
                FETCH_ALL: 'http://localhost/todo-app/api/v1/projects/%d/features',
                FETCH_BY_ID: 'http://localhost/todo-app/api/v1/projects/%d/features',
                UPDATE_BY_ID: 'http://localhost/todo-app/api/v1/projects/%d/features/%d?name=%s',
                STORE_NEW: 'http://localhost/todo-app/api/v1/projects/%d/features?name=%s',
                DELETE_BY_ID: 'http://localhost/todo-app/api/v1/projects/%d/features/%s'
            };
            this.getFeatures = function(id) {
                 var url = sprintf(URLS.FETCH_ALL, id);
                return $http.get(url);
            };
            this.getFeaturesById = function(id) {
                var url = sprintf(URLS.FETCH_BY_ID, id);
                return $http.get(url);
            };
            this.updateFeaturesById = function(id,id2,name){
                var url = sprintf(URLS.UPDATE_BY_ID, id,id2,name);
                return $http.put(url);                
            };
            this.addFeaturesById = function(id,name){
                var url = sprintf(URLS.STORE_NEW, id,name);
                return $http.post(url);                
            };
            this.deleteFeaturesById = function(proj_id,feat_ids){
                 var url = sprintf(URLS.DELETE_BY_ID, proj_id,feat_ids);
                return $http.delete(url); 
            };
        });
}());