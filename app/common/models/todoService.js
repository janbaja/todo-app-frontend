(function() {
    'use strict';

    angular.module('App.Service.Todo', [
        
    ])
        .service('TodoService', function($http) {
            var URLS = {
                FETCH_ALL: 'http://localhost/todo-app/api/v1/projects/%d/features/%d/todos',
                UPDATE_BY_ID: 'http://localhost/todo-app/api/v1/projects/%d/features/%d/todos/%id?name=%s',
                STORE_NEW: 'http://localhost/todo-app/api/v1/projects/%d/features/%d/todos?name=%s',
                DELETE_BY_ID: 'http://localhost/todo-app/api/v1/projects/%d/features/%d/todos/%s'
            };
            this.getTodos = function(id,feature_id) {
                 var url = sprintf(URLS.FETCH_ALL, id,feature_id);
                return $http.get(url);
            };         
            this.updateTodosById = function(proj_id,feat_id,todo_id,name){
                var url = sprintf(URLS.UPDATE_BY_ID, proj_id,feat_id,todo_id,name);
                return $http.put(url);                
            };
            this.addTodosById = function(proj_id,feat_id,name){
                var url = sprintf(URLS.STORE_NEW, proj_id,feat_id,name);
                return $http.post(url);                
            };
            this.deleteTodosById = function(proj_id,feat_id,todo_ids){
                 var url = sprintf(URLS.DELETE_BY_ID, proj_id,feat_id,todo_ids);
                return $http.delete(url); 
            };
        });
}());