(function() {
    'use strict';

    angular.module('App', [
        'ui.router',
        'ngAnimate',
        'App.Projects',
        'App.Projects.Features',
        'App.Projects.Features.Todos'
    ])
        .config(function($stateProvider, $urlRouterProvider){
            $stateProvider.state('main', {
                url: '',
                abstract: true
            });
            $urlRouterProvider.otherwise('/projects');
        });
}());